import 'package:flutter/material.dart';

const Color kcPrimaryColor = Color(0xFF9600FF);
const Color kcPrimaryColorDark = Color(0xFF300151);
const Color kcBlack = Color(0xFF000000);
const Color kcDarkGreyColor = Color(0xFF718096);
const Color kcMediumGrey = Color(0xFF474A54);
const Color kcLightGrey = Color.fromARGB(255, 187, 187, 187);
const Color kcVeryLightGrey = Color(0xFFE3E3E3);
const Color kcWhite = Color(0xFFFFFFFF);
const Color kcBackgroundColor = kcDarkGreyColor;
const Color kcLoginText = Color(0xFF319795);
const Color kcGreen = Color(0xFF81E6D9);
const Color kcDarkGreen = Color(0xFF319795);
const Color kcCircleGrey = Color(0xFFF7FAFC);

const Gradient kgTopLine = LinearGradient(
  colors: [Color(0xFF319795), Color(0xFF3182CE)],
);
const Gradient kgBackground = LinearGradient(
  colors: [Color(0xFFEBF4FF), Color(0xFFE6FFFA)],
);
const Gradient kgWave = LinearGradient(
  colors: [Color(0xFFE6FFFA), Color(0xFFEBF4FF)],
  begin: Alignment.topLeft,
  end: Alignment.bottomRight,
);
