class AppConstants {
  static const String deineJob = 'Deine Job\nwebsite';
  static const String login = 'Login';
  static const String kostenlos = 'Kostenlos Registrieren';
  static const String arbeitnehmer = 'Arbeitnehmer';
  static const String arberitgeber = 'Arbeitgeber';
  static const String temporarbuto = 'Temporärbüro';
  static const String dreiEinfache =
      'Drei einfache Schritte zu deinem neuen Job';
  static const String dreiEinfache2 =
      'Drei einfache Schritte zu deinem neuen Mitarbeiter';
  static const String dreiEinfache3 =
      'Drei einfache Schritte zur Vermittlung neuer Mitarbeiter';
  static const String erstellen1 = 'Erstellen dein Lebenslauf';
  static const String erstellen2 = 'Erstellen dein Lebenslauf';
  static const String mitNur = 'Mit nur einem Klick bewerben';
  static const String erstellen3 = 'Erstellen dein Unternehmensprofil';
  static const String erstellen4 = 'Erstellen ein Jobinserat';
  static const String wahleDeinen = 'Wähle deinen neuen Mitarbeiter aus';
  static const String erstellen5 = 'Erstellen dein Unternehmensprofil';
  static const String erhalte = 'Erhalte Vermittlungs- angebot von Arbeitgeber';
  static const String vermittlung =
      'Vermittlung nach Provision oder Stundenlohn';
}
