import 'package:stacked/stacked.dart';
import 'package:webview_ui/app/app.locator.dart';
import 'package:webview_ui/app/app.router.dart';
import 'package:stacked_services/stacked_services.dart';

class StartupViewModel extends BaseViewModel {
  final _routerService = locator<RouterService>();

  Future runStartupLogic() async {
    await Future.delayed(const Duration(seconds: 3));
    await _routerService.replaceWith(const HomeViewRoute());
  }
}
