import 'package:stacked/stacked.dart';
import 'package:webview_ui/ui/common/app_constants.dart';

class HomeViewModel extends BaseViewModel {
  String selectedTab = AppConstants.arbeitnehmer;
  bool tabButtonVisible = false;

  void setTab(String tab) {
    selectedTab = tab;
    notifyListeners();
  }

  void setVisibility(bool visibility) {
    tabButtonVisible = visibility;
    notifyListeners();
  }
}
