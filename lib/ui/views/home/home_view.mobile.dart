import 'package:flutter_svg/flutter_svg.dart';
import 'package:webview_ui/ui/common/app_colors.dart';
import 'package:webview_ui/ui/common/app_constants.dart';
import 'package:webview_ui/ui/common/ui_helpers.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:webview_ui/ui/views/home/widgets/widgets.dart';

import 'home_viewmodel.dart';

class HomeViewMobile extends ViewModelWidget<HomeViewModel> {
  const HomeViewMobile({super.key});

  @override
  Widget build(BuildContext context, HomeViewModel viewModel) {
    return Scaffold(
      backgroundColor: kcWhite,
      body: DecoratedBox(
        decoration: const BoxDecoration(gradient: kgBackground),
        child: SafeArea(
          child: Stack(
            children: [
              SingleChildScrollView(
                physics: const BouncingScrollPhysics(),
                child: Stack(
                  children: [
                    Center(
                      child: Column(
                        children: [
                          const SizedBox(height: 67),
                          verticalSpaceMedium,
                          const Text(
                            AppConstants.deineJob,
                            style: TextStyle(
                              fontSize: 42,
                              fontWeight: FontWeight.w500,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          SvgPicture.asset(
                            'assets/agreement.svg',
                            width: screenWidth(context),
                            fit: BoxFit.fitWidth,
                          )
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 556),
                      decoration: BoxDecoration(
                        borderRadius: const BorderRadius.vertical(
                          top: Radius.circular(12),
                        ),
                        boxShadow: [
                          BoxShadow(
                              color: kcMediumGrey.withOpacity(0.1),
                              spreadRadius: 1,
                              blurRadius: 3,
                              offset: const Offset(0, -6))
                        ],
                        color: Colors.white,
                      ),
                      child: Center(
                        child: Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(
                                top: 24,
                                left: 20,
                                right: 20,
                              ),
                              child: RegisterButton(
                                visibilityChanged: (value) =>
                                    viewModel.setVisibility(value),
                              ),
                            ),
                            const SizedBox(height: 64),
                            AppTabBar(
                              selected: viewModel.selectedTab,
                              titles: const [
                                AppConstants.arbeitnehmer,
                                AppConstants.arberitgeber,
                                AppConstants.temporarbuto,
                              ],
                              onSelect: (tab) => viewModel.setTab(tab),
                            ),
                            const SizedBox(height: 30),
                            if (viewModel.selectedTab ==
                                AppConstants.arbeitnehmer)
                              const ArbeitnehmerMobile()
                            else if (viewModel.selectedTab ==
                                AppConstants.arberitgeber)
                              const ArbeitgeberMobile()
                            else if (viewModel.selectedTab ==
                                AppConstants.temporarbuto)
                              const TemporaburoMobile()
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              CustomAppBar(
                children: [
                  !viewModel.tabButtonVisible
                      ? const Padding(
                          padding: EdgeInsets.only(right: 12),
                          child: SizedBox(
                            width: 200,
                            height: 40,
                            child: RegisterSecondaryButton(),
                          ),
                        )
                      : const SizedBox(),
                  const LoginButton()
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
