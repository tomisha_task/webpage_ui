import 'package:flutter/material.dart';
import 'package:visibility_detector/visibility_detector.dart';
import 'package:webview_ui/ui/common/app_colors.dart';
import 'package:webview_ui/ui/common/app_constants.dart';

class RegisterButton extends StatelessWidget {
  const RegisterButton({
    super.key,
    this.visibilityChanged,
  });

  final Function(bool)? visibilityChanged;

  @override
  Widget build(BuildContext context) {
    return VisibilityDetector(
      key: const Key('RegisterButton'),
      onVisibilityChanged: (VisibilityInfo info) {
        var visiblePercentage = info.visibleFraction * 100;
        visibilityChanged?.call(visiblePercentage == 100);
      },
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          gradient: kgTopLine,
          borderRadius: BorderRadius.circular(12),
        ),
        child: MaterialButton(
          onPressed: () {},
          child: const Text(
            AppConstants.kostenlos,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: kcWhite,
              fontWeight: FontWeight.w600,
              fontSize: 14,
            ),
          ),
        ),
      ),
    );
  }
}
