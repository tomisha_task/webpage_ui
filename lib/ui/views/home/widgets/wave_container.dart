import 'package:flutter/material.dart';
import 'package:webview_ui/ui/common/app_colors.dart';

class WaveContainer extends StatelessWidget {
  final Widget child;

  const WaveContainer({
    super.key,
    required this.child,
  });

  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: WaveClipper(),
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 80),
        decoration: const BoxDecoration(gradient: kgWave),
        child: child,
      ),
    );
  }
}

class WaveClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();

    // Top wave
    path.lineTo(0, size.height * 0.05);
    var firstControlPoint = Offset(size.width * 0.25, size.height * 0.0);
    var firstEndPoint = Offset(size.width * 0.5, size.height * 0.05);
    var secondControlPoint = Offset(size.width * 0.75, size.height * 0.1);
    var secondEndPoint = Offset(size.width, size.height * 0.05);

    path.quadraticBezierTo(firstControlPoint.dx, firstControlPoint.dy,
        firstEndPoint.dx, firstEndPoint.dy);
    path.quadraticBezierTo(secondControlPoint.dx, secondControlPoint.dy,
        secondEndPoint.dx, secondEndPoint.dy);

    // Draw line to bottom right
    path.lineTo(size.width, size.height * 0.95);

    // Bottom wave with 45-degree offset
    var thirdControlPoint = Offset(size.width * 0.75, size.height * 0.9);
    var thirdEndPoint = Offset(size.width * 0.5, size.height * 0.95);
    var fourthControlPoint = Offset(size.width * 0.25, size.height * 1.0);
    var fourthEndPoint = Offset(0, size.height * 0.95);

    path.quadraticBezierTo(thirdControlPoint.dx, thirdControlPoint.dy,
        thirdEndPoint.dx, thirdEndPoint.dy);
    path.quadraticBezierTo(fourthControlPoint.dx, fourthControlPoint.dy,
        fourthEndPoint.dx, fourthEndPoint.dy);

    path.lineTo(0, 0); // Move back to the top left
    path.close(); // Close the path

    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return false; // No need to reclip as the shape doesn't change
  }
}