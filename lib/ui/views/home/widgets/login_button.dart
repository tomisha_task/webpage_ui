import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:webview_ui/ui/common/app_colors.dart';

class LoginButton extends StatefulWidget {
  const LoginButton({super.key});

  @override
  State<LoginButton> createState() => _LoginButtonState();
}

class _LoginButtonState extends State<LoginButton> {
  bool _isHovered = false;

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      onEnter: (_) {
        setState(() {
          _isHovered = true;
        });
      },
      onExit: (_) {
        setState(() {
          _isHovered = false;
        });
      },
      child: GestureDetector(
        onTap: () {
          log('Login tapped');
        },
        child: Text(
          'Login',
          style: TextStyle(
            color: kcLoginText.withOpacity(_isHovered ? 0.8 : 1),
            fontSize: 14,
            fontWeight: FontWeight.w600,
            decorationColor: kcLoginText.withOpacity(0.8),
            decoration:
                _isHovered ? TextDecoration.underline : TextDecoration.none,
          ),
        ),
      ),
    );
  }
}
