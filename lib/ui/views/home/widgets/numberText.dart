import 'package:flutter/material.dart';
import 'package:webview_ui/ui/common/app_colors.dart';

class NumberText extends StatelessWidget {
  const NumberText({
    super.key,
    required this.title,
    required this.number,
  });

  final String title;
  final int number;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.baseline,
      textBaseline: TextBaseline.ideographic,
      children: [
        Text(
          '$number.',
          style: const TextStyle(
              fontSize: 130,
              fontWeight: FontWeight.w600,
              color: kcDarkGreyColor),
        ),
        Expanded(
          child: Text(
            title,
            style: const TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w600,
              color: kcDarkGreyColor,
            ),
          ),
        )
      ],
    );
  }
}
