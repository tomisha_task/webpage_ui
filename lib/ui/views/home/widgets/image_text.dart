import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:webview_ui/ui/views/home/widgets/widgets.dart';

class ImageText extends StatelessWidget {
  const ImageText({
    super.key,
    required this.title,
    required this.number,
    required this.image,
    this.isTop = true,
    this.isDesktop = false,
    this.enableCircle = false,
  });

  final String title;
  final int number;
  final String image;
  final bool isTop;
  final bool isDesktop;
  final bool enableCircle;

  @override
  Widget build(BuildContext context) {
    Widget widget = isDesktop
        ? Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 80,
              horizontal: 52,
            ),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                const Expanded(child: SizedBox()),
                number.isOdd
                    ? Expanded(
                        child: NumberText(
                          title: title,
                          number: number,
                        ),
                      )
                    : const SizedBox.shrink(),
                Expanded(
                  child: SvgPicture.asset(
                    image,
                    height: 255,
                  ),
                ),
                number.isEven
                    ? Expanded(
                        child: NumberText(
                          title: title,
                          number: number,
                        ),
                      )
                    : const SizedBox.shrink(),
                const Expanded(child: SizedBox()),
              ],
            ),
          )
        : Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Stack(
              alignment: Alignment.topCenter,
              children: [
                Padding(
                  padding: EdgeInsets.only(top: isTop ? 190 : 0),
                  child: SvgPicture.asset(
                    image,
                    height: 144,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: isTop ? 0 : 95),
                  child: NumberText(
                    title: title,
                    number: number,
                  ),
                ),
              ],
            ),
          );
    if (enableCircle) {
      widget = CircleBackgroundWidget(child: widget);
    }
    return widget;
  }
}
