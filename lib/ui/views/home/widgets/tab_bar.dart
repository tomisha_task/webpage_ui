import 'package:flutter/material.dart';
import 'package:webview_ui/ui/common/app_colors.dart';

class AppTabBar extends StatelessWidget {
  const AppTabBar({
    super.key,
    required this.titles,
    required this.selected,
    this.onSelect,
  });

  final List<String> titles;
  final String selected;
  final Function(String)? onSelect;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 45,
      child: ListView.builder(
        itemCount: titles.length,
        scrollDirection: Axis.horizontal,
        shrinkWrap: true,
        padding: const EdgeInsets.symmetric(horizontal: 20),
        itemBuilder: (context, index) {
          final bool isSelected = titles[index] == selected;
          final bool isFirst = titles.first == titles[index];
          final bool isLast = titles.last == titles[index];

          return GestureDetector(
            onTap: () => onSelect?.call(titles[index]),
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 32),
              decoration: BoxDecoration(
                  color: isSelected ? kcGreen : kcWhite,
                  border: Border.all(
                    width: 1,
                    color: isSelected ? kcGreen : kcLightGrey,
                  ),
                  borderRadius: (isFirst || isLast)
                      ? BorderRadius.horizontal(
                          left: Radius.circular(isFirst ? 12 : 0),
                          right: Radius.circular(isLast ? 12 : 0),
                        )
                      : null),
              child: Text(titles[index],
                  style: TextStyle(
                    color: isSelected ? kcWhite : kcDarkGreen,
                    fontWeight: FontWeight.w600,
                    fontSize: 14,
                  )),
            ),
          );
        },
      ),
    );
  }
}
