import 'package:flutter/material.dart';
import 'package:webview_ui/ui/common/app_colors.dart';

class CircleBackgroundWidget extends StatelessWidget {
  const CircleBackgroundWidget({
    super.key,
    required this.child,
  });

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned(
          left: MediaQuery.of(context).size.width * 0.33 - 200,
          child: const CircleAvatar(
            radius: 151,
            backgroundColor: kcCircleGrey,
            child: Icon(Icons.arrow_forward, color: Colors.white),
          ),
        ),
        child,
      ],
    );
  }
}
