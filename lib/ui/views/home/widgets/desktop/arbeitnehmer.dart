import 'package:flutter/material.dart';
import 'package:webview_ui/ui/common/app_colors.dart';
import 'package:webview_ui/ui/common/app_constants.dart';
import 'package:webview_ui/ui/common/ui_helpers.dart';
import 'package:webview_ui/ui/views/home/widgets/image_text.dart';
import 'package:webview_ui/ui/views/home/widgets/wave_container.dart';

class ArbeitnehmerDesktop extends StatelessWidget {
  const ArbeitnehmerDesktop({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ConstrainedBox(
          constraints: BoxConstraints(maxWidth: screenWidth(context) / 2),
          child: const Text(
            AppConstants.dreiEinfache,
            style: TextStyle(
              fontSize: 40,
              color: kcDarkGreyColor,
            ),
            textAlign: TextAlign.center,
          ),
        ),
        const SizedBox(height: 20),
        const ImageText(
          title: AppConstants.erstellen1,
          number: 1,
          image: 'assets/profile_data.svg',
          isTop: false,
          isDesktop: true,
        ),
        const WaveContainer(
          child: ImageText(
            title: AppConstants.erstellen2,
            number: 2,
            image: 'assets/task.svg',
            isDesktop: true,
          ),
        ),
        const ImageText(
          title: AppConstants.mitNur,
          number: 3,
          image: 'assets/personal_file.svg',
          isDesktop: true,
          enableCircle: true,
        ),
        const SizedBox(height: 32),
      ],
    );
  }
}
