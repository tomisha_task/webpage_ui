import 'package:flutter/material.dart';
import 'package:webview_ui/ui/common/app_colors.dart';
import 'package:webview_ui/ui/common/app_constants.dart';

class RegisterSecondaryButton extends StatelessWidget {
  const RegisterSecondaryButton({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        border: Border.all(
          color: kcLightGrey,
          width: 1,
        ),
        borderRadius: BorderRadius.circular(12),
      ),
      child: MaterialButton(
        onPressed: () {},
        child: const Text(
          AppConstants.kostenlos,
          textAlign: TextAlign.center,
          style: TextStyle(
            color: kcLoginText,
            fontWeight: FontWeight.w600,
            fontSize: 14,
          ),
        ),
      ),
    );
  }
}
