import 'package:flutter/material.dart';
import 'package:webview_ui/ui/common/app_constants.dart';
import 'package:webview_ui/ui/views/home/widgets/image_text.dart';
import 'package:webview_ui/ui/views/home/widgets/widgets.dart';

class TemporaburoMobile extends StatelessWidget {
  const TemporaburoMobile({super.key});

  @override
  Widget build(BuildContext context) {
    return const Column(
      children: [
        Text(
          AppConstants.dreiEinfache3,
          style: TextStyle(fontSize: 21),
          textAlign: TextAlign.center,
        ),
        SizedBox(height: 20),
        ImageText(
          title: AppConstants.erstellen5,
          number: 1,
          image: 'assets/profile_data.svg',
          isTop: false,
        ),
        SizedBox(height: 15),
        WaveContainer(
          child: ImageText(
            title: AppConstants.erhalte,
            number: 2,
            image: 'assets/job_offers.svg',
          ),
        ),
        ImageText(
          title: AppConstants.vermittlung,
          number: 3,
          image: 'assets/business_deal.svg',
          enableCircle: true,
        ),
        SizedBox(height: 32),
      ],
    );
  }
}
