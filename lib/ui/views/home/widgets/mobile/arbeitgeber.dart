import 'package:flutter/material.dart';
import 'package:webview_ui/ui/common/app_constants.dart';
import 'package:webview_ui/ui/views/home/widgets/image_text.dart';
import 'package:webview_ui/ui/views/home/widgets/widgets.dart';

class ArbeitgeberMobile extends StatelessWidget {
  const ArbeitgeberMobile({super.key});

  @override
  Widget build(BuildContext context) {
    return const Column(
      children: [
        Text(
          AppConstants.dreiEinfache2,
          style: TextStyle(
            fontSize: 21,
          ),
          textAlign: TextAlign.center,
        ),
        SizedBox(height: 20),
        ImageText(
          title: AppConstants.erstellen3,
          number: 1,
          image: 'assets/profile_data.svg',
          isTop: false,
        ),
        SizedBox(height: 15),
        WaveContainer(
          child: ImageText(
            title: AppConstants.erstellen4,
            number: 2,
            image: 'assets/about_me.svg',
          ),
        ),
        ImageText(
          title: AppConstants.wahleDeinen,
          number: 3,
          image: 'assets/swipe_profiles.svg',
          enableCircle: true,
        ),
        SizedBox(height: 32),
      ],
    );
  }
}
