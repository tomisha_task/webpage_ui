import 'package:flutter/material.dart';
import 'package:webview_ui/ui/common/app_constants.dart';
import 'package:webview_ui/ui/views/home/widgets/image_text.dart';
import 'package:webview_ui/ui/views/home/widgets/widgets.dart';

class ArbeitnehmerMobile extends StatelessWidget {
  const ArbeitnehmerMobile({super.key});

  @override
  Widget build(BuildContext context) {
    return const Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Text(
            AppConstants.dreiEinfache,
            style: TextStyle(
              fontSize: 21,
            ),
            textAlign: TextAlign.center,
          ),
        ),
        SizedBox(height: 20),
        ImageText(
          title: AppConstants.erstellen1,
          number: 1,
          image: 'assets/profile_data.svg',
          isTop: false,
        ),
        WaveContainer(
          child: ImageText(
            title: AppConstants.erstellen2,
            number: 2,
            image: 'assets/task.svg',
          ),
        ),
        ImageText(
          title: AppConstants.mitNur,
          number: 3,
          image: 'assets/personal_file.svg',
          enableCircle: true,
        ),
        SizedBox(height: 32),
      ],
    );
  }
}
