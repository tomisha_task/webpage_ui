import 'package:flutter_svg/flutter_svg.dart';
import 'package:webview_ui/ui/common/app_colors.dart';
import 'package:webview_ui/ui/common/app_constants.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:webview_ui/ui/views/home/widgets/widgets.dart';

import 'home_viewmodel.dart';

class HomeViewDesktop extends ViewModelWidget<HomeViewModel> {
  const HomeViewDesktop({super.key});

  @override
  Widget build(BuildContext context, HomeViewModel viewModel) {
    return Scaffold(
      backgroundColor: kcWhite,
      body: DecoratedBox(
        decoration: const BoxDecoration(gradient: kgBackground),
        child: Center(
          child: Stack(
            children: [
              SingleChildScrollView(
                physics: const BouncingScrollPhysics(),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 167),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          const Expanded(child: SizedBox()),
                          Expanded(
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                const Text(
                                  AppConstants.deineJob,
                                  style: TextStyle(
                                    fontSize: 65,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                const SizedBox(height: 53),
                                SizedBox(
                                  height: 50,
                                  width: 320,
                                  child: RegisterButton(
                                    visibilityChanged: (value) =>
                                        viewModel.setVisibility(value),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(width: 70),
                          ClipRRect(
                            borderRadius: BorderRadius.circular(1000),
                            child: ColoredBox(
                              color: kcWhite,
                              child: SvgPicture.asset(
                                'assets/agreement.svg',
                                width: 455,
                                fit: BoxFit.contain,
                              ),
                            ),
                          ),
                          const Expanded(child: SizedBox())
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 90),
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.vertical(
                          top: Radius.circular(12),
                        ),
                        color: Colors.white,
                      ),
                      child: Center(
                        child: Column(
                          children: [
                            const SizedBox(height: 64),
                            AppTabBar(
                              selected: viewModel.selectedTab,
                              titles: const [
                                AppConstants.arbeitnehmer,
                                AppConstants.arberitgeber,
                                AppConstants.temporarbuto,
                              ],
                              onSelect: (tab) => viewModel.setTab(tab),
                            ),
                            const SizedBox(height: 55),
                            if (viewModel.selectedTab ==
                                AppConstants.arbeitnehmer)
                              const ArbeitnehmerDesktop()
                            else if (viewModel.selectedTab ==
                                AppConstants.arberitgeber)
                              const ArbeitgeberDesktop()
                            else if (viewModel.selectedTab ==
                                AppConstants.temporarbuto)
                              const TemporaburoDesktop()
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              CustomAppBar(children: [
                !viewModel.tabButtonVisible
                    ? const Padding(
                        padding: EdgeInsets.only(right: 30),
                        child: SizedBox(
                          width: 255,
                          height: 40,
                          child: RegisterSecondaryButton(),
                        ),
                      )
                    : const SizedBox(),
                const LoginButton()
              ]),
            ],
          ),
        ),
      ),
    );
  }
}
